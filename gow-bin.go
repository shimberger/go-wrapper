package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type projectInfo struct {
	name string
	root string
}

const appname = "gow"
const projectFile = ".gowproject"

func existsFile(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func getProjectInfo(currdir string) projectInfo {
	projdir := currdir
	var contents []byte
	var err error
	for !existsFile(filepath.Join(projdir, projectFile)) {
		if projdir == "/" {
			fmt.Println("No gow enabled project found")
			os.Exit(1)
		}
		projdir = filepath.Dir(projdir)
	}
	if contents, err = ioutil.ReadFile(filepath.Join(projdir, projectFile)); err != nil {
		fmt.Println("Could not read project file")
		os.Exit(1)
	}
	return projectInfo{string(contents), projdir}
}

func createSelfSymlink(project projectInfo) {
	parts := strings.Split(project.name, "/")
	pathParts := parts[:len(parts)-1]
	extPath := filepath.Join(pathParts...)
	packagePath := filepath.Join(project.root, "ext", "src", extPath)
	os.MkdirAll(packagePath, os.ModeDir|os.ModePerm)
	os.Symlink(project.root, filepath.Join(packagePath, parts[len(parts)-1]))
}

func run(command string, args ...string) error {
	cmd := exec.Command(command, args...)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	err = cmd.Start()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	go io.Copy(os.Stdout, stdout)
	go io.Copy(os.Stderr, stderr)
	return cmd.Wait()
}

func main() {
	var err error = nil
	args := os.Args
	currdir := ""
	if currdir, err = os.Getwd(); err != nil {
		log.Fatalf("Could not get working directory")
	}
	if len(args) >= 2 && args[1] == "init" {
		if len(args) == 2 || args[2] == "" {
			fmt.Println("Please enter an absoulte project root like github.com/yourname/myproject as second parameter")
			os.Exit(1)
		}
		os.MkdirAll(filepath.Join("ext", "src"), os.ModeDir|os.ModePerm)
		gitignore, err := os.Create(filepath.Join("ext", "src", ".gitignore"))
		if err != nil {
			fmt.Println("Could not create .gitignore file")
			os.Exit(1)
		}
		defer gitignore.Close()
		fmt.Fprintf(gitignore, "*\n")
		fmt.Fprintf(gitignore, "!.gitignore\n")
		projfile, err := os.Create(projectFile)
		if err != nil {
			fmt.Println("Could not create project file")
			os.Exit(1)
		}
		defer projfile.Close()
		fmt.Fprintf(projfile, args[2])
		fmt.Println("Project created")
		os.Exit(0)
	}

	project := getProjectInfo(currdir)
	createSelfSymlink(project)

	os.Setenv("GOPATH", filepath.Join(project.root, "ext"))
	os.Setenv("PATH", filepath.Join(project.root, "ext", "bin")+":"+os.Getenv("PATH"))

	if len(args) >= 2 && args[1] == "subl" {
		run("subl", project.root)
	} else {
		run("go", args[1:]...)
	}

}
