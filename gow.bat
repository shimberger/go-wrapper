@ECHO OFF

SET appname="gow"
SET gotool="go"
SET sublime="subl"
SET extdir="ext"
SET projfile=".gowproject"

CD >> %TEMP%\%appname%.cd
SET /p currdir= < %TEMP%\%appname%.cd
DEL %TEMP%\%appname%.cd

IF "%1" == "init" (
  IF "%2" == "" (
    ECHO "Please enter an absoulte project root like github.com/yourname/myproject as second parameter"
    EXIT
  )
  MKDIR %extdir%
  ECHO '*'		>> %currdir%%extdir%\.gitignore
  ECHO '!.gitignore'	>> %currdir%%extdir%\.gitignore
  ECHO %2 >> %currdir%\%projfile%
  EXIT
)



